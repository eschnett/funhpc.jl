module FunsTest

using Base.Test

using  Funs



function main()
    fun_untyped()
    fun_typed()
    mac_untyped()
    mac_typed()
end

function fun_untyped()
    # Function
    v1 = call(inc, 1)
    @test v1 == 2
    
    # DataType
    v2 = call(Inc, 2)
    @test v2.i == 3
    
    # Fun
    v3 = call(incf, 3)
    @test v3 == 4
end

function fun_typed()
    # Function
    v1 = call(R=Int, inc, 1)
    @test v1 == 2
    
    # DataType
    v2 = call(R=Inc, Inc, 2)
    @test v2.i == 3
    
    # Fun
    v3 = call(R=Int, incf, 3)
    @test v3 == 4
end

function mac_untyped()
    # Function
    v1 = @call inc(1)
    @test v1 == 2
    
    # DataType
    v2 = @call Inc(2)
    @test v2.i == 3
    
    # Fun
    v3 = @call incf(3)
    @test v3 == 4
end

function mac_typed()
    # Function
    v1 = @call Int inc(1)
    @test v1 == 2
    
    # DataType
    v2 = @call Inc Inc(2)
    @test v2.i == 3
    
    # Fun
    v3 = @call Int incf(3)
    @test v3 == 4
end

function inc(i::Integer)
    i+1
end

immutable Inc
    i::Int
    Inc(i) = new(i+1)
end

incf = Fun{Int}(inc)



main()

end
