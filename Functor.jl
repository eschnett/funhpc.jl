module Functor

using Funs

export fmap

fmap{FT}(f::Callable, xs::FT, yss...; R::Type=eltype(f)) =
    error("fmap not specialized for type $FT")



# Array

# TODO: Use a stagedfunction for this

# function fmap{T,D}(f::Callable, xs::Array{T,D}, yss::Array...;
#                    R::Type=eltype(f))
#     [@assert size(ys) == size(xs) for ys in yss]
#     rs = similar(xs, R)
#     @simd for i in 1:length(xs)
#         @inbounds rs[i] = @call f(xs[i], map(ys->ys[i], yss)...)
#     end
#     rs::Array{R,D}
# end
function fmap{T,D}(f::Callable, xs::Array{T,D};
                   R::Type=eltype(f))
    rs = similar(xs, R)
    @simd for i in 1:length(xs)
        @inbounds rs[i] = @call f(xs[i])
    end
    rs::Array{R,D}
end
function fmap{T,D}(f::Callable, xs::Array{T,D}, ys1::Array;
                   R::Type=eltype(f))
    @assert size(ys1) == size(xs)
    rs = similar(xs, R)
    @simd for i in 1:length(xs)
        @inbounds rs[i] = @call f(xs[i], ys1[i])
    end
    rs::Array{R,D}
end
function fmap{T,D}(f::Callable, xs::Array{T,D},
                   ys1::Array, ys2::Array, yss::Array...;
                   R::Type=eltype(f))
    @assert size(ys1) == size(xs)
    @assert size(ys2) == size(xs)
    [@assert size(ys) == size(xs) for ys in yss]
    rs = similar(xs, R)
    @simd for i in 1:length(xs)
        @inbounds rs[i] = @call f(xs[i], ys1[i], ys2[i], map(ys->ys[i], yss)...)
    end
    rs::Array{R,D}
end



# Fun (Callable)

function fmap(f::Callable, fun::Callable, funs::Callable...; R::Type=eltype(f))
    Fun{R}((x,ys...) ->
           call(R=R, f, call(fun,x), map((fun,y)->call(fun,y), funs, ys)...))
end



# Set

function fmap{T}(f::Callable, xs::Set{T}; R::Type=eltype(f))
    rs = Set{R}()
    for x in xs
        push!(rs, @call f(x))
    end
    rs::Set{R}
end



# Tuple

function fmap(f::Callable, xs::Tuple, yss::Tuple...; R::Type=eltype(f))
    [@assert length(ys) == length(xs) for ys in yss]
    Rs = ntuple(i->R, length(xs))
    map((xs...)->call(f,xs...), xs, yss...)::Rs
end

end
