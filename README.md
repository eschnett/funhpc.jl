# FunHPC.jl #

FunHPC.jl is Functional High-Performance Computing, implemented in the
Julia language. "Functional" refers to a programming style that avoids
shared mutable state, as advertised e.g. in the Haskell language.

### Functional Programming ###

Shared mutable state refers to objects that can be modified by
multiple entities (functions, other objects, threads) simultaneously.
This requires careful thought, so that the object always remains in a
valid state; typically, one uses either mutexes/locks, atomic
operations, or transactions for this. Unfortunately, these mechanisms
are expensive (slow), and are difficult to use correctly.

In a *functional programming model*, objects are only modified while
they are created. Later, once they are visible to other
functions/threads, they remain immutable, which avoids access
conflicts. This is even more convenient in a distributed environment,
where objects may be copied to remote locations to be accessed there.

Functional programming has long been identified as a prime candidate
for making parallel or distributed programming easier and safer.

### Getting Started ###

To get started with FunHPC.jl, you need to install Julia
<http://julialang.org/> and its MPI bindings
<https://github.com/lcw/MPI.jl>.
