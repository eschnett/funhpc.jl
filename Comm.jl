module Comm

using Funs, MPI

import Base.LPROC, Base.PGRP

export ProcID, procid
export myproc, procs, nprocs
export run_main
export rexec, @rexec
export rexec_everywhere, @rexec_everywhere

typealias ProcID Int

const OPTIMIZE_SELF_COMMUNICATION = true

global COMM
global RANK, SIZE

function init()
    MPI.Init()
    global COMM = MPI.COMM_WORLD       # Comm_dup?
    global RANK = MPI.Comm_rank(COMM)
    global SIZE = MPI.Comm_size(COMM)
    # Override some Base settings to describe our multi-process setup
    LPROC.id = myproc()
    PGRP.workers = collect(procs())
end

function finalize()
    # Undo override above to prevent errors during shutdown
    LPROC.id = 1
    PGRP.workers = []
    MPI.Finalize()
end

myproc() = RANK+1
nprocs() = SIZE
procs() = 1:SIZE

global USE_RECV_LOOP
global STOP_SENDING = false
global STOP_RECEIVING = false
const TAG = 0
const META_TAG = 1

# TODO: Use TestSome
function send_item(p::Integer, t::Integer, item)
    @assert USE_RECV_LOOP
    req = MPI.isend(item, p-1, t, COMM)
    while t==META_TAG || !STOP_SENDING
        flag, status = MPI.Test!(req)
        if flag return end
        yield()
    end
    # MPI.cancel(req)
end

function recv_item(p::Integer, t::Integer)
    @assert USE_RECV_LOOP
    while t==META_TAG || !STOP_RECEIVING
        flag, item, status = MPI.irecv(p==0 ? MPI.ANY_SOURCE : p-1, t, COMM)
        if flag return item end
        yield()
    end
end

function terminate()
    @assert USE_RECV_LOOP
    # Determine parents and children for a binary tree
    pmax = div(myproc(), 2)
    pmin = max(pmax, 1)
    cmin = 2*myproc()
    cmax = min(2*myproc()+1, nprocs())
    # Stage 1: Stop sending
    # Wait for termination message from parent
    @sync for p in pmin:pmax
        @async recv_item(p, META_TAG)
    end
    # Stop sending
    global STOP_SENDING = true
    # Send termination message to children
    @sync for c in cmin:cmax
        @async send_item(c, META_TAG, nothing)
    end
    # Wait for termination confirmation from children
    @sync for c in cmin:cmax
        @async recv_item(c, META_TAG)
    end
    # Send termination confirmation to parent
    @sync for p in pmin:pmax
        @async send_item(p, META_TAG, nothing)
    end
    # Stage 2: Stop receiving
    # Wait for second termination message from parent
    @sync for p in pmin:pmax
        @async recv_item(p, META_TAG)
    end
    global STOP_RECEIVING = true
    # Send second termination message to children
    @sync for c in cmin:cmax
        @async send_item(c, META_TAG, nothing)
    end
end



function run_main(main::Callable; run_main_everywhere::Bool=false)
    init()
    global USE_RECV_LOOP = !(OPTIMIZE_SELF_COMMUNICATION && nprocs()==1)
    r = nothing
    @sync begin
        if USE_RECV_LOOP
            @async recv_loop()
        end
        if run_main_everywhere || myproc()==1
            r = call(main)
        end
        if USE_RECV_LOOP
            terminate()
        end
    end
    finalize()
    r
end

function recv_loop()
    @assert USE_RECV_LOOP
    while !STOP_RECEIVING
        run_task(recv_item(0, TAG))
    end
end

function run_task(item)
    if !STOP_SENDING
        # (f::Callable, args...) = item
        @schedule call(item...)
    end
end



# TODO: Support sending arbitrary expressions, not just function calls
# TODO: Use a stagedfunction for this

# function rexec(p::Integer, f::Callable, args...)
#     item = tuple(f, args...)
#     if OPTIMIZE_SELF_COMMUNICATION && p == myproc()
#         run_task(item)
#     else
#         @schedule send_item(p, TAG, item)
#     end
# end
function rexec(p::Integer, f::Callable)
    item = tuple(f)
    if OPTIMIZE_SELF_COMMUNICATION && p == myproc()
        run_task(item)
    else
        @schedule send_item(p, TAG, item)
    end
end
function rexec(p::Integer, f::Callable, arg1)
    item = tuple(f, arg1)
    if OPTIMIZE_SELF_COMMUNICATION && p == myproc()
        run_task(item)
    else
        @schedule send_item(p, TAG, item)
    end
end
function rexec(p::Integer, f::Callable, arg1, arg2)
    item = tuple(f, arg1, arg2)
    if OPTIMIZE_SELF_COMMUNICATION && p == myproc()
        run_task(item)
    else
        @schedule send_item(p, TAG, item)
    end
end
function rexec(p::Integer, f::Callable, arg1, arg2, arg3)
    item = tuple(f, arg1, arg2, arg3)
    if OPTIMIZE_SELF_COMMUNICATION && p == myproc()
        run_task(item)
    else
        @schedule send_item(p, TAG, item)
    end
end
function rexec(p::Integer, f::Callable, arg1, arg2, arg3, arg4, args...)
    item = tuple(f, arg1, arg2, arg3, arg4, args...)
    if OPTIMIZE_SELF_COMMUNICATION && p == myproc()
        run_task(item)
    else
        @schedule send_item(p, TAG, item)
    end
end

function _rexec_tree(item)
    pmin = 2*myproc()
    pmax = min(2*myproc()+1, nprocs())
    for p in pmin:pmax
        rexec(p, _rexec_tree, item)
    end
    # (f::Callable, args...) = item
    call(item...)
end
function rexec_everywhere(f::Callable, args...)
    item = tuple(f, args...)
    rexec(1, _rexec_tree, item)
end

macro rexec(p, expr)
    esc(Base.localize_vars(:(rexec($p, ()->$expr)), false))
end

macro rexec_everywhere(expr)
    esc(Base.localize_vars(:(rexec_everywhere(()->$expr)), false))
end

end
