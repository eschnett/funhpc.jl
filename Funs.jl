module Funs

using Base.Meta

import Base.eltype

export Fun
export Callable
export call, @call, eltype



# TODO: Use Base.return_types
eltype(f::Function) = Any
call(f::Function, args...; R::Type=Any) = f(args...)::R

eltype(f::DataType) = f
call(f::DataType, args...; R::Type=f) = f(args...)::R

immutable Fun{T}
    fun::Function
end
eltype{R}(f::Fun{R}) = R
call{T}(f::Fun{T}, args...; R::Type=T) = f.fun(args...)::R

typealias Callable Union(Function, DataType, Fun)
# call, eltype

iscall(expr) = isexpr(expr, :call)
# Note: The called function is args[1], the arguments are args[2:end]
getcallargs(expr) = (@assert iscall(expr); expr.args)

macro call(args...)
    if length(args) == 1
        (expr,) = args
        callargs = getcallargs(expr)
        esc(:(call($(callargs...))))
    elseif length(args) == 2
        (R, expr) = args
        callargs = getcallargs(expr)
        esc(:(call(R=$R, $(callargs...))))
    else
        error("Expected: @call [<type>] <f>(<args>...)")
    end
end

end
