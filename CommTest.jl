module CommTest

using Base.Test

using Comm

function main()
    local_fun()
    local_fun2()
    local_fun_everywhere()
    local_mac()
    local_mac2()
    local_mac_everywhere()
end

function local_fun()
    @test myproc() == 1
    global DONE = false
    rexec(mod1(2, nprocs()), remote1, 1, "a", [1.0])
    while !DONE yield() end
end

function local_fun2()
    @test myproc() == 1
    global DONE = false
    i=1
    rexec(mod1(2, nprocs()), ()->(s="a"; remote1(i, s, [1.0])))
    while !DONE yield() end
end

function local_fun_everywhere()
    global COUNTER = 0
    rexec_everywhere(inc)
    while COUNTER < nprocs() yield() end
end

function local_mac()
    while !DONE yield() end
    global DONE = false
    @rexec mod1(2, nprocs()) remote1(1, "a", [1.0])
    while COUNTER < nprocs() yield() end
end

function local_mac2()
    while !DONE yield() end
    global DONE = false
    i=1
    @rexec mod1(2, nprocs()) (s="a"; remote1(i, s, [1.0]))
    while COUNTER < nprocs() yield() end
end

function local_mac_everywhere()
    global COUNTER = 0
    @rexec_everywhere inc()
    while COUNTER < nprocs() yield() end
end

function remote1(i, s, a)
    @test myproc() == (nprocs() < 2 ? 1 : 2)
    @test i == 1
    @test s == "a"
    @test a == [1.0]
    rexec(nprocs(), remote2)
end

function remote2()
    @test myproc() == nprocs()
    rexec(1, ()->remote3())
end

function remote3()
    @test myproc() == 1
    global DONE = true
end

function inc()
    # rexec(1, () -> global COUNTER += 1)
    rexec(1, inc1)
end
function inc1()
    global COUNTER += 1
end



run_main(main)

end
