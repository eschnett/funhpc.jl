module FunctorTest

using Base.Test

using Functor, Funs

function main()
    test_array()
    test_fun()
    test_set()
    test_tuple()
end

function test_array()
    xs = [1,2,4,8,16]
    ys = [1,2,3,4,5]
    @test fmap(x->2*x, xs) == [2,4,8,16,32]
    @test fmap(+, xs, ys) == [2,4,7,12,21]
    x2s = [1 2 4; 8 16 32]
    y2s = [1 2 3; 4 5 6]
    @test fmap(x->2*x, x2s) == [2 4 8; 16 32 64]
    @test fmap(+, x2s, y2s) == [2 4 7; 12 21 38]
end

function test_fun()
    f = Fun{Float64}(x::Int -> float(x)+0.5)
    g = Fun{Complex{Float64}}(x::Float64 -> x+im)
    @test (@call fmap(g, f)(12)) == 12.5+im
    f2 = Fun{Int}(x::Int -> x+1)
    g2 = Fun{Complex{Float64}}((x::Float64, y::Int) -> x+float(y)*im)
    @test (@call fmap(g2, f, f2)(12,13)) == 12.5+14.0im
end

function test_set()
    xs = Set([1,2,4,8,16])
    @test fmap(x->2*x, xs) == Set([2,4,8,16,32])
end

function test_tuple()
    xs = (1,2,4,8,16)
    ys = (1,2,3,4,5)
    @test fmap(x->2*x, xs) == (2,4,8,16,32)
    @test fmap(+, xs, ys) == (2,4,7,12,21)
end

main()

end
