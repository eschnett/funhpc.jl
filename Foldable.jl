module Foldable

using Funs

export freduce

freduce{Z,FT}(f::Callable, zero::Z, xs::FT, yss...; R::Type=eltype(f)) =
    error("freduce not specialized for types ($Z,$FT)")



# Array

function freduce{Z,T,D}(f::Callable, zero::Z, xs::Array{T,D}, yss::Array...;
                        R::Type=eltype(f))
    [@assert size(ys) == size(xs) for ys in yss]
    r = zero
    for i in 1:length(xs)
        #=@inbounds=# r = @call f(r, xs[i], map(ys->ys[i], yss)...)
    end
    r::R
end



# Set

function freduce{Z,T}(f::Callable, zero::Z, xs::Set{T}; R::Type=eltype(f))
    r = zero
    for x in xs
        #=@inbounds=# r = @call f(r, x)
    end
    r::R
end



# Tuple

function freduce{Z}(f::Callable, zero::Z, xs::Tuple, yss::Tuple...;
                    R::Type=eltype(f))
    [@assert length(ys) == length(xs) for ys in yss]
    r = zero
    for i in 1:length(xs)
        #=@inbounds=# r = @call f(r, xs[i], map(ys->ys[i], yss)...)
    end
    r::R
end

end
